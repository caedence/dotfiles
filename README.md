<img src="media/desktop.png">

# Caedence's desktop configuration files.

This is a repository for my personal dotfiles.

## Specs
- OS:             Arch Linux x86-64 [Link](https://archlinux.org/)
- WM:             xmonad [Link](https://xmonad.org/)
- Panel:          xmobar [Link](https://hackage.haskell.org/package/xmobar), trayer [Link](https://github.com/sargon/trayer-srg)
- Terminal:       Alacritty [Link](https://github.com/alacritty/alacritty)
- Notifications:  dunst [Link](https://github.com/dunst-project/dunst)
- Launcher:       rofi [Link](https://github.com/davatorium/rofi)
- Editor:         vim [Link](https://www.vim.org/)
------
- GTK Theme:      Arc-Dark-Gotham [Link](https://github.com/0xhjohnson/arc-theme-gotham)
- File Manager:   Nautilus (GNOME Files) [Link](https://gitlab.gnome.org/GNOME/nautilus)
- Icon Set:       Papirus Icons [Link](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme)
------
- Screenshots:    shotgun [Link](https://github.com/neXromancers/shotgun), slop [Link](https://github.com/naelstrof/slop) (Through a script: See .xmonad/scripts)
- Wallpapers:     nitrogen [Link](https://github.com/l3ib/nitrogen/)
------
Fonts Used:
- Impallari Cabin Font [Link](https://github.com/impallari/Cabin)
- Font Awesome 5 Free/Brands [Link](https://fontawesome.com/)
- Hack [Link](https://sourcefoundry.org/hack/)

