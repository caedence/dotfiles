-- Caedence's XMonad Configuration
-- Last Edited: 06/07/2022
--
-- This is my xmonad configuration for my personal desktop. I'm no Haskell guru, but I have tried to keep things at least neat and tidy.
-- 

import XMonad
import System.IO (hPutStrLn)
import System.Directory
import System.Exit

import Data.List
import Data.Monoid
import Data.Maybe (fromJust)

import XMonad.Util.EZConfig
import XMonad.Util.SpawnOnce
import XMonad.Util.Run
import XMonad.Util.Ungrab (unGrab)

import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.DynamicLog

import XMonad.Layout.SimplestFloat
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed

import XMonad.Layout.LayoutModifier
import XMonad.Layout.Renamed
import XMonad.Layout.NoBorders
import XMonad.Layout.LayoutModifier
import XMonad.Layout.Spacing
import XMonad.Layout.Simplest
import XMonad.Layout.WindowNavigation
import XMonad.Layout.ShowWName

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

------------------------------------------------------------------------
-- User Variables

-- The preferred terminal program
myTerminal :: String
myTerminal = "alacritty"

-- modMask is set to "Super" key. (Windows Key)
myModMask :: KeyMask
myModMask = mod4Mask

-- Application Variables
myLauncher :: String
myLauncher = "rofi -modi window,drun,run -show drun -font 'Cabin Bold 12'"
-- myLauncher = "ulauncher"

myAppSwitch :: String
myAppSwitch = "rofi -modi window,drun,run -show window -font 'Cabin Bold 12'"

myRunWindow :: String
myRunWindow = "rofi -modi window,drun,run -show run -font 'Cabin Bold 12'"

myEditor :: String
myEditor = myTerminal ++ " -e vim"

myFileManager :: String
myFileManager = "nautilus"

myTaskManager :: String
myTaskManager = "alacritty -e btop"

myKeysWindow :: String
myKeysWindow = "$HOME/.xmonad/scripts/keybinds.py"

terminalMusicPlayer :: String
terminalMusicPlayer = "alacritty -e ncmpcpp"

myRSS :: String
myRSS = "alacritty -e newsboat"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
myBorderWidth :: Dimension
myBorderWidth  = 1

-- Workspace settings
myWorkspaces       = [" main ", " web ", " chat ", " sys ", " mus ", " vid ", " gfx ", " doc ", " etc "]
myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..] -- (,) == \x y -< (x,y)

-- Makes workspaces clickable.
clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
    where i = fromJust $ M.lookup ws myWorkspaceIndices

-- Border colors for unfocused and focused windows, respectively.
myNormalBorderColor = "#3b4252"
myFocusedBorderColor = "#33859e"

------------------------------------------------------------------------
-- Key bindings. Add, modify or remove key bindings here.

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
     [ ((modm,               xK_Return), spawn $ XMonad.terminal conf)       -- Launch a Terminal
     , ((modm .|. shiftMask, xK_Return), spawn myLauncher)                   -- Launch rofi as a program launcher
     , ((modm,               xK_Tab   ), spawn myAppSwitch)                  -- Launch rofi as a application switch (Similar to Windows Alt-Tab)
     , ((modm .|. shiftMask, xK_Tab   ), spawn myRunWindow)                  -- Launch rofi as a run command window
     , ((modm .|. shiftMask, xK_b     ), spawn "dmenu_run")                  -- Launch dmenu as a program launcher
     , ((modm,               xK_Insert), spawn myEditor)                     -- Launch an editor
     , ((modm .|. shiftMask, xK_Page_Up), spawn myTaskManager)               -- Launch a task manager of choice (Default: Set to btop in alacritty)
     , ((modm,               xK_Page_Up), spawn myRSS)                       -- Launch newsboat, an RSS news aggregator
     , ((modm,               xK_Page_Down), spawn terminalMusicPlayer)       -- Launch ncmpcpp, a ncurses MPD client
     , ((modm,               xK_q     ), kill)                               -- Close focused Window
     , ((modm,               xK_space ), sendMessage NextLayout)             -- Rotate through Layouts
     , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf) -- Reset layouts to default
     , ((modm,               xK_n     ), refresh)                            -- Resize viewed windows to correct size
     , ((modm,               xK_j     ), windows W.focusDown)                -- Move Focus to Next
     , ((modm,               xK_k     ), windows W.focusUp  )                -- Move Focus to Previous
     , ((modm,               xK_m     ), windows W.focusMaster  )            -- Move Focus to Master
     , ((modm,               xK_s), windows W.swapMaster)                    -- Swap Focused with Master
     , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )               -- Swap Focused with Next
     , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )               -- Swap Focused with Previous
     , ((modm,               xK_h     ), sendMessage Shrink)                 -- Shrink Master area
     , ((modm,               xK_l     ), sendMessage Expand)                 -- Expand Master area
     , ((modm,               xK_t     ), withFocused $ windows . W.sink)     -- Push Window to tiling
     , ((modm              , xK_comma ), sendMessage (IncMasterN 1))         -- Increment windows in Master
     , ((modm              , xK_period), sendMessage (IncMasterN (-1)))      -- Deincrement windows in Master
     , ((modm              , xK_Delete     ), sendMessage ToggleStruts)      -- Toggles the gap between the panel and the windows (See avoidStruts from X.H.ManageDocks in xmonad documentation)
     , ((modm           , xK_Home     ), spawn myFileManager)                -- Launch a file manager
     , ((modm           , xK_backslash), spawn myKeysWindow)                 -- Show keybinds python script
     , ((modm .|. shiftMask, xK_e     ), io (exitWith ExitSuccess))          -- Quit xmonad
    , ((modm              , xK_e     ), spawn "xmonad --recompile; xmonad --restart")  -- Restart xmonad

    -- Shotgun Screenshot Tool bindings (Using shell script)
    , ((modm                 , xK_Print ), unGrab *> safeSpawn ".xmonad/scripts/shotclip" ["-f"]) -- Fullscreen
    , ((modm .|. controlMask , xK_Print ), unGrab *> safeSpawn ".xmonad/scripts/shotclip" ["-s"]) -- Selection
    , ((modm .|. shiftMask   , xK_Print ), unGrab *> safeSpawn ".xmonad/scripts/shotclip" ["-w"]) -- Focused Window
    ]
    ++
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++
    -- mod-{i,o,p}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{i,o,p}, Move client to screen 1, 2, or 3
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_i, xK_o, xK_p] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
-- Set to Defaults
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

------------------------------------------------------------------------
-- Layouts:

-- Makes spacing easier to use
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Variation, with no borders
mySpacingBorderless :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacingBorderless i = spacingRaw True (Border i i i i) True (Border i i i i) True

-- Theme for showWName which prints current workspace when you change workspaces.
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:Cabin:bold:size=60"
    , swn_fade              = 1
    , swn_bgcolor           = "#0a0f14"
    , swn_color             = "#ffffff"
    }

-- Tab themes for the Tabbed layout.
myTabTheme :: Theme
myTabTheme = def { fontName            = "xft:Cabin:bold:size=10"
                 , activeColor         = "#131c25"
                 , inactiveColor       = "#0a0f14"
                 , activeBorderColor   = "#131c25"
                 , inactiveBorderColor = "#0a0f14"
                 , activeTextColor     = "#d3dae3"
                 , inactiveTextColor   = "#0a3749"
                 }

-- Layout Configuration
tall                  = renamed [Replace "Master/Stack"]
                       $ smartBorders
                       $ windowNavigation
                       $ mySpacing 5
                       $ ResizableTall 1 (3/100) (1/2) []

full                  = renamed [Replace "Fullscreen"]

floats                = renamed [Replace "Floating"]
                       $ smartBorders
                       $ windowNavigation
                       $ simplestFloat

tabs                  = renamed [Replace "Tabbed"]
                       $ smartBorders
                       $ windowNavigation
                       $ mySpacing 5
                       $ tabbed shrinkText myTabTheme

myLayout =  avoidStruts $ myDefaultLayout
  where
    myDefaultLayout =     showWS (
                          tall
                      ||| tabs
                      ||| noBorders Full
                      ||| floats )

    showWS = showWName' myShowWNameTheme

------------------------------------------------------------------------
-- Window rules:

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = manageDocks <+> composeAll
    [ className =? "gw2-64.exe"                              --> doCenterFloat    -- Not sure if Wine applications work.
    , className =? "Org.gnome.Nautilus"                      --> doCenterFloat
    , className =? "Xmessage"                                --> doCenterFloat
    , title     =? "galculator"                              --> doCenterFloat
    , resource  =? "Dialog"                                  --> doCenterFloat
    , resource  =? "desktop_window"                          --> doIgnore
    , resource  =? "kdesktop"                                --> doIgnore
    , className =? "bottles"                                 --> doShift " doc "
    , className =? "virt-manager"                            --> doShift " etc "
    , className =? "obs"                                     --> doShift " vid "
    , className =? "firefox"                                 --> doShift " web "
    , className =? "Steam"                                   --> doShift " gfx "
    , className =? "discord"                                 --> doShift " chat "
    , className =? "io.github.celluloid_player.Celluloid"    --> doShift " vid "
    , className =? "mpv"                                     --> doShift " vid "
    , className =? "vlc"                                     --> doShift " vid "
    , className =? "ario"                                    --> doShift " mus "
    , title     =? "Spotify"                                 --> doShift " mus "  -- Spotify isn't playing nice, still looking into it.
    , title     =? "Keybinds"                                --> doFloat
    , isFullscreen                                           --> doFullFloat
    ]

------------------------------------------------------------------------
-- Event handling (C: This may become useful later)

-- myEventHook = 

------------------------------------------------------------------------
-- Startup hook

myStartupHook = do
    spawnOnce "nvidia-settings --load-config-only"                          -- NVIDIA Proprietary Driver settings on startup
    spawnOnce "nm-applet"                                                   -- Network Manager tray applet
    spawnOnce "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &" -- Gnome Polkit Authentication Agent
    spawnOnce "xsetroot -cursor_name left_ptr"                              -- Defaulting current cursor theme to desktop. (Removes the "X" when no windows are open)
    spawnOnce "solaar -w hide"                                              -- Solaar, Logitech Peripheral Management Application
    spawnOnce "picom &"                                                     -- Picom Compositor (Formerly compton)
    spawnOnce "nitrogen --restore"                                          -- Nitrogen Wallpaper Setter
    spawnOnce "pasystray &"                                                 -- PulseAudio System Tray App
    spawnOnce "dunst &"                                                     -- Dunst Notification Daemon

-- Trayer System Tray (Yes I know, the string is obnoxious.)
    spawnOnce "trayer --edge bottom --align right --widthtype request --margin 196 --distance 7 --padding 6 --iconspacing 1 --SetDockType true --SetPartialStrut true --expand true --monitor 1  --transparent true --alpha 0 --tint 0x0a0f14 --height 24 &"

------------------------------------------------------------------------
-- Main function: Start xmonad

main :: IO ()
main = do
    xmproc0 <- spawnPipe "xmobar -x 0 .config/xmobar/xmobarrc0"
    xmproc1 <- spawnPipe "xmobar -x 1 .config/xmobar/xmobarrc1"
    xmonad $ ewmh def
        { manageHook = myManageHook
        , handleEventHook = docksEventHook <+> fullscreenEventHook <+> ewmhDesktopsEventHook
        , modMask = myModMask
        , startupHook = myStartupHook
        , layoutHook = myLayout
        , workspaces = myWorkspaces
        , keys = myKeys
        , terminal = myTerminal
        , focusFollowsMouse = myFocusFollowsMouse
        , borderWidth = myBorderWidth
        , normalBorderColor = myNormalBorderColor
        , focusedBorderColor = myFocusedBorderColor
        , logHook = dynamicLogWithPP $ xmobarPP
              { ppOutput = \x -> hPutStrLn xmproc0 x >> hPutStrLn xmproc1 x
              , ppCurrent = xmobarColor "#d3dae3" "" . wrap "[" "]" -- Current Workspace in xmobar
              , ppVisible = xmobarColor "#d3dae3" "" . wrap "(" ")" . clickable -- Visible Workspace - A workspace with a window on it
              , ppHiddenNoWindows = xmobarColor "#0a3749" "" . clickable
              , ppHidden = xmobarColor "#33859e" "" . clickable -- Hidden Workspace - A non-visible workspace with windows inside
              , ppTitle = xmobarColor "#d3dae3" "" -- Title of the active window
              , ppSep = "<fc=#d8dee9> | </fc>" -- Seperator Character
              , ppUrgent = xmobarColor "#c23127" "" . wrap "!" "!" -- Urgent Workspace
              }
        }
