#!/usr/bin/python
# Keybindings - A small GTK program to display xmonad keybindings
# Written by Scarlett Messing (Caedence) 09/07/2021
# Last edited: 11/08/2021

import gi
import cairo

gi.require_version("Gtk", "3.0")
gi.require_version("Gdk", "3.0")

from gi.repository import Gtk, Gdk

class Window(Gtk.Window):
	def __init__(self):
		super().__init__(title="Keybinds")
	
		screen = self.get_screen()
		visual = screen.get_rgba_visual()
		context = Gtk.StyleContext()
		box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, halign=Gtk.Align.START, spacing=0)
		self.connect("draw", self.draw)
		css_provider = Gtk.CssProvider()
#		This line points to ~/.config/gtk-3.0/gtk.css
		css_provider.new()
		context.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
	
		self.move(1565, 30)	
		self.set_default_size(350, 275)

		label = Gtk.Label(label=" [Super] + Return: Open Terminal\n [Super] + [Shift] + Return: Open Launcher\n [Super] + Space: Toggle Between Layouts\n [Super] + [Shift] + Space: Reset Layouts to Default\n [Super] + Q: Kill Focused Window\n [Super] + [Ctrl] + E: Restart xmonad\n [Super] + [Shift] + E: Exit xmonad (Logoff)\n [Super] + N: Resize viewed windows to correct size\n [Super] + J: Move Focus Next\n [Super] + K: Move Focus Previous\n [Super] + M: Move Focus to Master\n [Super] + [Shift] + J: Swap Window Next\n [Super] + [Shift] + K: Swap Window Previous\n [Super] + H: Shrink Master\n [Super] + L: Expand Master\n [Super] + T: Push Window to Tiling\n [Super] + [Comma]: Increment Master Windows\n [Super] + [Period]: Decrement Master Window", )

#		This name is set in your gtk.css file in ~/.config/gtk-3.0/gtk.css
#		Example: "label#key-fonts { font: 12px monospace; }
		label.set_name("key-fonts")
		box.pack_start(label, False, False, 5)
		self.add(box)

		if screen.is_composited():
			self.set_visual(visual),
#			box.set_opacity(0.1),
#			label1.set_opacity(1)

		self.set_app_paintable(True)
		self.show_all()
				
	def draw(self, widget, context):
		context.set_source_rgba(0.2, 0.2, 0.2, 0.9)
		context.set_operator(cairo.OPERATOR_SOURCE)
		context.paint()
		context.set_operator(cairo.OPERATOR_OVER)

win = Window()
win.connect("destroy", Gtk.main_quit)
Gtk.main()
